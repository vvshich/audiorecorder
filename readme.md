# AudioRecorder
![title](/uploads/923ce5c22cea0f0ad92dbdcfcfc95c6e/title.jpg)

This application records your voice and saves recordered to .wav file by adress: ```{application folder}/record/```, but you can also choose your directory.

## How it looks
#### Static
![img](/uploads/00c76a5a6ceed57dd585d1cc497c475c/main.jpg)
#### When recording
![img](/uploads/61bef6a4f9174263a42feeddf91be3ae/main_recording.jpg)

## [Settings](https://gitlab.com/vspirate/audiorecorder/-/tree/master/src/windows/settings)
At the moment you can here:
- Choose folder
- Choose input
- Choose quality

![img](/uploads/c008fcc424889738886f21fe28f0efef/settings.jpg)


Built with C++ and Qt.
