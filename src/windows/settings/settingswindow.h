#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include "../main/mainwindow.h"
#include <QtMultimedia>

class AudioSettingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AudioSettingsWindow(QWidget *parent = nullptr);
    ~AudioSettingsWindow();

    void setWindow( MainWindow *W ) { thisWindow = W; fileName = W->showOutput(); }

private slots:
    void on_OutputBtn_clicked();

    // signals for show main window
    void on_CloseBtn_clicked() { thisWindow->show(); close(); }
    void on_AudioSettingsWindow_finished() { thisWindow->show(); }

    void on_Inputs_activated(const QString&);

    void on_Qualities_activated(const QString&);

private:
    Ui::AudioSettingsWindow *ui;
    MainWindow *thisWindow;
    QString fileName;
    QString input;
    QMultimedia::EncodingQuality quality;
};

#endif // SETTINGSWINDOW_H
