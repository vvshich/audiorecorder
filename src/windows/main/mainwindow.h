#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

// to record
#include <QAudio>
#include <QAudioInput>
#include <QAudioRecorder>

// to save files
#include <QFile>
#include <QFileDialog>
#include <QPixmap>    // for icons

// to check recording time
#include <QTimer>
#include "src/timer/timer.h"

typedef bool check;
namespace Ui {
class AudioSettingsWindow;
class MainWindow;
}

class AudioSettingsWindow;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setQuality(QMultimedia::EncodingQuality& q) { quality = q; }

    void setOutput(QString& F) { path = F; numberOfFile = -1; }

    void setInput(QString& I) { currentInput = I; }

    QString showOutput() const { return path; }

private slots:
    void on_RecBtn_clicked();

    void on_PauseBtn_clicked();

    void on_RecBtn_pressed();

    void on_RecBtn_released();

    void on_SettingsButton_clicked();

    void on_horizontalSlider_sliderMoved(int position);

private:
    Ui::MainWindow *ui;
    AudioSettingsWindow* dialog;
    inline void setIconForRecordButton(const QString&);
    inline void setIconForPauseButton(const QString&);

    // main in this application
    QAudioRecorder Recorder;
    QMultimedia::EncodingQuality quality;
    QAudioEncoderSettings& SetUpFormat();
    QString currentInput;
    qreal currentVolume;

    int numberOfFile = 1;
    QString path;
    QDir folder;
    void setDefaultPath();

    // Timer logic
    check isRecording = false;
    check isPause = false;
    timer::time recordedTime;
    QTimer* timer = new QTimer();
    void updateUiRecordedTime();
};

#endif // MAINWINDOW_H
