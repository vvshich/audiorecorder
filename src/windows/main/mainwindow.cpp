#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../settings/settingswindow.h"
#include "../../button/buttonhover.h"
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPixmap record(":record/Record.png");
    ui->RecBtn->setIcon(record);

    connect(timer, &QTimer::timeout, this, &MainWindow::updateUiRecordedTime);
    ui->PauseBtn->setEnabled(false);

    currentVolume = 1;
    currentInput = Recorder.audioInput();
    quality = QMultimedia::VeryHighQuality;

    ButtonHoverWatcher* watcher = new ButtonHoverWatcher(this, ":/settings/SettingsHover.png", ":/settings/Settings.png");
    ui->SettingsButton->installEventFilter(watcher);

    folder.mkdir(QDir::currentPath() + "/record/");
    path = QDir::currentPath() + "/record/record" + QString::number(numberOfFile) + ".wav";
    std::string g = path.toStdString();
    setDefaultPath();
}

// ============= FOR DESIGN BUTTONS =============

void MainWindow::setIconForRecordButton(const QString& icon)
{
    const QPixmap* record = new QPixmap(icon);
    ui->RecBtn->setIcon(*record);
    delete record;
}

void MainWindow::setIconForPauseButton(const QString& icon)
{
    const QPixmap* record = new QPixmap(icon);
    ui->PauseBtn->setIcon(*record);
    delete record;
}

void MainWindow::on_RecBtn_pressed()
{
    setIconForRecordButton(":record/Red.png");
}

void MainWindow::on_RecBtn_released()
{
    QPixmap *record = isRecording ? new QPixmap(":record/Recording.png") : new QPixmap(":record/Record.png");
    ui->RecBtn->setIcon(*record);
    delete record;
}


void MainWindow::updateUiRecordedTime()
{
    recordedTime.update();

    QString Format = "";
    recordedTime.toNormalFormat(recordedTime.getHours(), Format);

    Format.push_back(':');
    recordedTime.toNormalFormat(recordedTime.getMinutes(), Format);

    Format.push_back(':');
    recordedTime.toNormalFormat(recordedTime.getSeconds(), Format);

    ui->TimeLabel->setText("Recorded: " + Format);
}


// ============= MAIN BUTTONS =============

// first push to record voice
// second push to stop recording
void MainWindow::on_RecBtn_clicked()
{
    if (0 <= numberOfFile)
        setDefaultPath();

    if( isRecording ) {

        setIconForRecordButton(":record/Record.png");
        ui->PauseBtn->setEnabled(false);
        ui->SettingsButton->setEnabled(true);

        timer->stop();
        recordedTime.clear();

        isRecording = false;
        Recorder.stop();

    }else {
        setIconForRecordButton(":record/Recording.png");
        ui->PauseBtn->setEnabled(true);
        ui->SettingsButton->setEnabled(false);

        QAudioEncoderSettings settings = SetUpFormat();

        Recorder.setEncodingSettings(settings, QVideoEncoderSettings());
        Recorder.setOutputLocation(QUrl::fromLocalFile(path));
        Recorder.setAudioInput(currentInput);
        Recorder.setVolume(currentVolume);

        isRecording = true;
        Recorder.record();
        timer->start(1000);
        updateUiRecordedTime();
    }

}

void MainWindow::on_PauseBtn_clicked()
{
    if (!isPause) {
        Recorder.pause();
        timer->stop();
        isPause = true;
        setIconForPauseButton(":pause/Pause.png");
        return;
    }
    Recorder.record();
    timer->start(1000);
    isPause = false;
    setIconForPauseButton(":pause/Resume.png");
}

void MainWindow::on_horizontalSlider_sliderMoved(int position)
{
    currentVolume = position / 100;
    Recorder.setVolume(currentVolume);
    Recorder.setVolume(currentVolume);
}

// ============= SETTINGS =============

// move to setting window
void MainWindow::on_SettingsButton_clicked()
{
    hide();
    dialog = new AudioSettingsWindow(this);
    dialog->setWindow(this);
    dialog->show();
}

QAudioEncoderSettings& MainWindow::SetUpFormat()
{
    QAudioEncoderSettings *settings = new QAudioEncoderSettings();
    settings->setSampleRate(48000);
    settings->setChannelCount(1);
    settings->setQuality(quality);
    return *settings;
}

void MainWindow::setDefaultPath()
{
    numberOfFile = 0;
    do {
        numberOfFile++;
        path = QDir::currentPath() + "/record/record" + QString::number(numberOfFile) + ".wav";
    } while(QFileInfo::exists(path));
}

MainWindow::~MainWindow()
{
    delete timer;
    delete ui;
}
