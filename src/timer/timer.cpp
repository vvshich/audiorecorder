#include "timer.h"

void timer::time::toNormalFormat(int v, QString& Format)
{
    if (v >= 10) {
        Format.push_back(QString::number(v));
    } else {
        Format.push_back('0');
        Format.push_back(QString::number(v));
    }
};

void timer::time::clear()
{
    this->sec   = 0;
    this->min   = 0;
    this->hours = 0;
}

void timer::time::update()
{
    this->sec++;

    // update minutes and hours
    if(this->sec == 60) {
        this->sec = 0;
        this->min++;
    }
    if (this->min == 60) {
        this->min = 0;
        this->hours++;
    }
}
